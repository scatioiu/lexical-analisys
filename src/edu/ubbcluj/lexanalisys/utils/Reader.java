package edu.ubbcluj.lexanalisys.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Reader {

    public static ArrayList<String> readData(String path) {
        ArrayList<String> content = new ArrayList<>();
        ArrayList<String> content1 = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(path))) {
            content = stream.collect(Collectors.toCollection(ArrayList::new));
            content.forEach(item -> {
                String[] atoms = item.split(" ");
                for (String atom : atoms) {
                    content1.add(atom);
                }
                });
            } catch(IOException e){
                e.printStackTrace();
            }
        System.out.println(content1.get(5));
            return content1;
        }
    }
