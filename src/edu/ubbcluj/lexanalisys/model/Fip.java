package edu.ubbcluj.lexanalisys.model;


import java.util.ArrayList;

public class Fip {
    private ArrayList<Triplet<String, Integer, Integer>> fip;

    public Fip() {
        this.fip = new ArrayList<>();
    }

    public void addElement(String atom, int code, int pos) {
        fip.add(new Triplet<>(atom, code, pos));
    }
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        fip.forEach(item -> {
            stringBuilder.append(item.getT());
            stringBuilder.append(" ");
            stringBuilder.append(item.getT1());
            stringBuilder.append(" ");
            stringBuilder.append(item.getT2());
            stringBuilder.append(System.getProperty("line.separator"));
        });
        return stringBuilder.toString();
    }
}
