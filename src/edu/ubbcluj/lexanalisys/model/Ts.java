package edu.ubbcluj.lexanalisys.model;

import java.util.ArrayList;
import java.util.Set;

public class Ts {
    private ArrayList<Pair<String, Integer>> ts;

    public Ts() {
        this.ts=new ArrayList<>();
    }

    public void addElement(String atom, int code) {
        if (getAtomPosition(atom) == -1)
            ts.add(new Pair(atom, code));

    }

    public int getAtomPosition(String atom) {

        Pair<String, Integer> t = ts.stream().filter(item ->
                atom.equals(item.getT())).findAny().orElse(null);
        return t == null ? -1 : ts.indexOf(t);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        ts.forEach(item -> {
            stringBuilder.append(item.getT());
            stringBuilder.append(" ");
            stringBuilder.append(item.getT1());
            stringBuilder.append(System.getProperty("line.separator"));
        });
        return stringBuilder.toString();
    }
}
