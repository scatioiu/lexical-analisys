package edu.ubbcluj.lexanalisys.model;

public class Triplet<T,T1,T2> {
    private T t;
    private T1 t1;
    private  T2 t2;

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public T1 getT1() {
        return t1;
    }

    public void setT1(T1 t1) {
        this.t1 = t1;
    }

    public T2 getT2() {
        return t2;
    }

    public void setT2(T2 t2) {
        this.t2 = t2;
    }

    public Triplet(T t, T1 t1, T2 t2) {
        this.t = t;
        this.t1 = t1;
        this.t2 = t2;
    }
}
