package edu.ubbcluj.lexanalisys.service;

import edu.ubbcluj.lexanalisys.model.Fip;
import edu.ubbcluj.lexanalisys.model.Pair;
import edu.ubbcluj.lexanalisys.model.Triplet;
import edu.ubbcluj.lexanalisys.model.Ts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

public class Analizer {
    public Pair<Fip, Ts> analyze(ArrayList<String> sourceCode, ArrayList<String> keywords,ArrayList<String>operators){
        Fip fip = new Fip();
        Ts ts = new Ts();
        int[] q = {0};
        int[] l = {1};
        Pattern identifiers = Pattern.compile("[a-zA-Z]");
        Pattern constraints = Pattern.compile("[1-9]");
        sourceCode.forEach(item -> {
            String[] atoms = item.split(" ");
            Arrays.stream(atoms).forEach(atom -> {
                atom=atom.replaceAll("\\s","");
               // System.out.println(atom);
                if (keywords.contains(atom) || operators.contains(atom)) {
                    fip.addElement(atom, q[0], -1);
                    q[0]++;
                } else if (identifiers.matcher(atom).matches() || constraints.matcher(atom).matches() || atom.equals("0")) {
                    ts.addElement(atom, q[0]);
                    fip.addElement(atom, q[0], ts.getAtomPosition(atom));
                    q[0]++;

                } else {
                    System.err.println("Lexical error at line: " + l[0]+""+atom);
                    q[0]++;
                }
            });
            l[0]++;
        });

        return new Pair<>(fip,ts);
    }
}
