package edu.ubbcluj.lexanalisys.controller;

import edu.ubbcluj.lexanalisys.model.Fip;
import edu.ubbcluj.lexanalisys.model.Pair;
import edu.ubbcluj.lexanalisys.model.Ts;
import edu.ubbcluj.lexanalisys.service.Analizer;
import edu.ubbcluj.lexanalisys.utils.Reader;

import java.nio.file.Paths;
import java.util.ArrayList;

public class Controller {
    private ArrayList<String> sourcecode;
    private ArrayList<String> keywords;
    private ArrayList<String> operators;
    private Analizer analizer;

    public Controller() {
       // System.out.println(Paths.get(ClassLoader.getSystemResource("program1.txt").getPath()));
        this.sourcecode = Reader.readData("/home/electrix/Desktop/Lexical Analysis/src/edu/ubbcluj/lexanalisys/controller/resources/program1.txt");
        this.keywords = Reader.readData("/home/electrix/Desktop/Lexical Analysis/src/edu/ubbcluj/lexanalisys/controller/resources/keywords.txt");
        this.operators = Reader.readData("/home/electrix/Desktop/Lexical Analysis/src/edu/ubbcluj/lexanalisys/controller/resources/operators.txt");
this.analizer = new Analizer();
    }

    public void printTables() {

        Pair<Fip, Ts> pair = this.analizer.analyze(sourcecode, keywords, operators);
        System.out.println(pair.getT().toString());
        System.out.println(pair.getT1().toString());
    }
}
